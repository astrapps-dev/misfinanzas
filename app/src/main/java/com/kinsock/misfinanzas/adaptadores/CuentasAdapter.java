package com.kinsock.misfinanzas.adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.database.Cuenta;

import java.util.List;

/**
 * Created by vick on 13/01/16.
 */
public class CuentasAdapter extends RecyclerView.Adapter<CuentasAdapter.CuentaViewHolder> {

    Context context;
    ItemClickListener listener;
    /**
     * Lista de objetos {@link Cuenta} que representan la fuente de datos
     * de inflado
     */
    private List<Cuenta> items;

    public CuentasAdapter(List<Cuenta> items, Context context, ItemClickListener listener) {
        this.items = items;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public CuentaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_cuentas, viewGroup, false);
        return new CuentaViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CuentaViewHolder viewHolder, int i) {
        viewHolder.textViewCuenta.setText(items.get(i).cuenta);
        double monto = Cuenta.getMonto(items.get(i));
        viewHolder.textViewMonto.setText("$ " + String.valueOf(monto));

        if (monto > 0) {
            viewHolder.textViewMonto.setTextColor(context.getResources().getColor(R.color.color_monto_positivo));
        } else {
            viewHolder.textViewMonto.setTextColor(context.getResources().getColor(R.color.color_monto_negativo));
        }

        viewHolder.iconView.setInitials(false);
        viewHolder.iconView.setLetterSize(18);
        viewHolder.iconView.setLettersNumber(2);
        viewHolder.iconView.setShapeColor(items.get(i).color);
        viewHolder.iconView.setLetter(String.valueOf(items.get(i).cuenta));
    }

    public interface ItemClickListener {
        void onClick(View view, int position, List<Cuenta> items);
    }

    public class CuentaViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public MaterialLetterIcon iconView;
        public TextView textViewCuenta;
        public TextView textViewMonto;

        public CuentaViewHolder(View v) {
            super(v);
            iconView = (MaterialLetterIcon) v.findViewById(R.id.iconView);
            textViewCuenta = (TextView) v.findViewById(R.id.textViewCuenta);
            textViewMonto = (TextView) v.findViewById(R.id.textViewMonto);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v, getAdapterPosition(), items);
        }
    }
}