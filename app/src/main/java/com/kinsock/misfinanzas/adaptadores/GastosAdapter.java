package com.kinsock.misfinanzas.adaptadores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.database.Gasto;

import java.util.List;

/**
 * Created by vick on 14/01/16.
 */
public class GastosAdapter extends RecyclerView.Adapter<GastosAdapter.GastoViewHolder> {

    Context context;
    ItemClickListener listener;
    /**
     * Lista de objetos {@link Gasto} que representan la fuente de datos
     * de inflado
     */
    private List<Gasto> items;

    public GastosAdapter(List<Gasto> items, Context context, ItemClickListener listener) {
        this.items = items;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public GastoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_gastos, viewGroup, false);
        return new GastoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GastoViewHolder viewHolder, int i) {
        viewHolder.fechaView.setText(items.get(i).fecha);
        viewHolder.montoView.setText("$ " + items.get(i).monto);
        viewHolder.razonView.setText(items.get(i).razon);

        viewHolder.iconView.setInitials(false);
        viewHolder.iconView.setLetterSize(18);
        viewHolder.iconView.setLettersNumber(2);
        viewHolder.iconView.setShapeColor(items.get(i).cuenta.color);
        viewHolder.iconView.setLetter(String.valueOf(items.get(i).cuenta.cuenta));

    }

    public interface ItemClickListener {
        void onClick(View view, int position, List<Gasto> items);
    }

    public class GastoViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        // Campos respectivos de un item
        public MaterialLetterIcon iconView;
        public TextView fechaView;
        public TextView montoView;
        public TextView razonView;

        public GastoViewHolder(View v) {
            super(v);
            iconView = (MaterialLetterIcon) v.findViewById(R.id.iconView);
            fechaView = (TextView) v.findViewById(R.id.fechaView);
            montoView = (TextView) v.findViewById(R.id.montoView);
            razonView = (TextView) v.findViewById(R.id.razonView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(v, getAdapterPosition(), items);
        }
    }
}

