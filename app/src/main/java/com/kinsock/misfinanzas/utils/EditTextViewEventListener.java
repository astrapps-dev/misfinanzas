package com.kinsock.misfinanzas.utils;

public abstract class EditTextViewEventListener {

    public abstract void userIsTyping();

}
