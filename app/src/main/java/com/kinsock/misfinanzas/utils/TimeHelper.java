package com.kinsock.misfinanzas.utils;

import android.content.Context;

import com.kinsock.misfinanzas.R;

/**
 * Created by vick on 7/03/16.
 */
public class TimeHelper {

    public static String getMonth(int month, Context context) {
        switch (month) {
            case 1:
                return context.getString(R.string.month01);
            case 2:
                return context.getResources().getString(R.string.month02);
            case 3:
                return context.getResources().getString(R.string.month03);
            case 4:
                return context.getResources().getString(R.string.month04);
            case 5:
                return context.getResources().getString(R.string.month05);
            case 6:
                return context.getResources().getString(R.string.month06);
            case 7:
                return context.getResources().getString(R.string.month07);
            case 8:
                return context.getResources().getString(R.string.month08);
            case 9:
                return context.getResources().getString(R.string.month09);
            case 10:
                return context.getResources().getString(R.string.month10);
            case 11:
                return context.getResources().getString(R.string.month11);
            case 12:
                return context.getResources().getString(R.string.month12);
            default:
                return String.valueOf(month);
        }
    }
}
