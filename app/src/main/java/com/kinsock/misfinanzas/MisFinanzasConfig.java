package com.kinsock.misfinanzas;

/**
 * Created by vick on 26/02/16.
 */
public class MisFinanzasConfig {

    // tracking id for Google Analytics,
    // leave this constant empty if you do not want to use Google Analytics
    public static final String ANALYTICS_TRACKING_ID = "UA-54316053-5";

    // unit ids for AdMob,
    // leave these constants empty if you do not want to use AdMob
    public static final String ADMOB_UNIT_ID = "ca-app-pub-6863993738022836/4780807101";
    public static final String ADMOB_UNIT_ID_POI_DETAIL = "ca-app-pub-XXXXXXXXXXXXXXXXXXXXXXXXXXX";
    public static final String ADMOB_UNIT_ID_MAP = "ca-app-pub-XXXXXXXXXXXXXXXXXXXXXXXXXXX";

    // test device id for AdMob,
    // setup this constant if you want to avoid invalid impressions,
    // you can find your hashed device id in the logcat output by requesting an ad when debugging on your device
    public static final String ADMOB_TEST_DEVICE_ID = "6865295FE0326333F8B6722262CAE264";

}
