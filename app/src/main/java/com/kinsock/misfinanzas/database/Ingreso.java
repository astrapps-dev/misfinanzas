package com.kinsock.misfinanzas.database;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Ingresos", id = "_id")
public class Ingreso extends Model {

    @Column(name = "cuenta")
    public Cuenta cuenta;

    @Column(name = "fecha")
    public String fecha;

    @Column(name = "razon")
    public String razon;

    @Column(name = "monto")
    public String monto;

    public Ingreso() {
        super();
    }

    public Ingreso(Cuenta cuenta, String fecha, String razon, String monto) {
        super();
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.razon = razon;
        this.monto = monto;
    }

    public static List<Ingreso> getAll() {
        // This is how you execute a query
        return new Select()
                .from(Ingreso.class)
                .orderBy("fecha ASC")
                .execute();
    }

    public static List<Ingreso> getAllbyCuenta(Cuenta cuenta) {
        // This is how you execute a query
        return new Select()
                .from(Ingreso.class)
                .where("cuenta = ?", cuenta.getId())
                .execute();
    }

    public static String getCuenta(Cuenta cuenta) {
        Cuenta c = new Select()
                .from(Cuenta.class)
                .where("Cuenta = ?", cuenta.cuenta)
                .executeSingle();
        return c.cuenta;
    }

    public static Ingreso getIngreso(Long id) {
        return new Select()
                .from(Ingreso.class)
                .where("_id = ?", id)
                .executeSingle();
    }

    public static double getTotalIngresos() {
        double monto = 0;
        List<Ingreso> ingresoList = Ingreso.getAll();
        for (int f = 0; f < ingresoList.size(); f++) {
            monto = monto + Double.parseDouble(ingresoList.get(f).monto);
        }

        return monto;
    }
}
