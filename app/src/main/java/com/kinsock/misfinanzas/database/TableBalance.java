package com.kinsock.misfinanzas.database;

/**
 * Created by vick on 22/08/15.
 */
public class TableBalance {

    public static final String TABLE_NAME = "balance";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_CUENTA = "cuenta";
    public static final String FIELD_MONTO = "monto";
    public static final String FIELD_ICONO = "icono";
    public static final String CREATE_DB_TABLE = "create table " + TABLE_NAME + "( " +
            FIELD_ID + " integer primary key autoincrement," +
            FIELD_CUENTA + " text," +
            FIELD_MONTO + " text," +
            FIELD_ICONO + " text"
            + " );";
    public static final String DEFAULT_SORT =
            FIELD_ID + " ASC";
    // These are the rows that we will retrieve.
    public static final String[] ALL_PROJECTION = new String[]{
            FIELD_ID,
            FIELD_CUENTA,
            FIELD_MONTO,
            FIELD_ICONO
    };
    private int id;
    private String cuenta;
    private double monto;
    private int icono;

    //--------------------------------------------------------------------------//

    public TableBalance() {
    }

    public TableBalance(String cuenta, double monto, int icono) {
        this.cuenta = cuenta;
        this.monto = monto;
        this.icono = icono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getIcono() {
        return icono;
    }

    public void setIcono(int icono) {
        this.icono = icono;
    }

    public static class IndexColumns {
        public static final int FIELD_ID = 0;
        public static final int FIELD_CUENTA = 1;
        public static final int FIELD_MONTO = 2;
        public static final int FIELD_ICONO = 3;
    }
}
