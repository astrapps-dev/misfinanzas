package com.kinsock.misfinanzas.database;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Cuentas", id = "_id")
public class Cuenta extends Model {

    @Column(name = "cuenta")
    public String cuenta;

    @Column(name = "color")
    public int color;

    public Cuenta() {
        super();
    }

    public Cuenta(String cuenta, int color) {
        super();
        this.cuenta = cuenta;
        this.color = color;
    }

    public static List<Cuenta> getAll() {
        return new Select()
                .from(Cuenta.class)
                .orderBy("cuenta ASC")
                .execute();
    }

    public static Cuenta getCuenta(Long id) {
        return new Select()
                .from(Cuenta.class)
                .where("_id = ?", id)
                .executeSingle();
    }

    public static double getDineroTotal() {
        return Ingreso.getTotalIngresos() - Gasto.getTotalGastos();
    }

    public static void deleteAllInAccount(Cuenta c){
        List<Ingreso> ingresos = Ingreso.getAllbyCuenta(c);
        for (int f=0; f< ingresos.size(); f++){
            ingresos.get(f).delete();
        }
        List<Gasto> gastos = Gasto.getAllbyCuenta(c);
        for (int f=0; f< gastos.size(); f++){
            gastos.get(f).delete();
        }
        c.delete();
    }

    public static double getMonto(Cuenta cuenta) {
        double monto = 0;
        List<Ingreso> ingresoList = Ingreso.getAllbyCuenta(cuenta);
        for (int f = 0; f < ingresoList.size(); f++) {
            monto = monto + Double.parseDouble(ingresoList.get(f).monto);
        }
        List<Gasto> gastoList = Gasto.getAllbyCuenta(cuenta);
        for (int f = 0; f < gastoList.size(); f++) {
            monto = monto - Double.parseDouble(gastoList.get(f).monto);
        }

        return monto;
    }

    public static boolean tieneMovimientos(Cuenta cuenta) {
        List<Ingreso> ingresoList = Ingreso.getAllbyCuenta(cuenta);
        List<Gasto> gastoList = Gasto.getAllbyCuenta(cuenta);
        return ingresoList.size() > 0 || gastoList.size() > 0;
    }
}
