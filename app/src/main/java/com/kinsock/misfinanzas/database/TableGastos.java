package com.kinsock.misfinanzas.database;

/**
 * Created by vick on 22/08/15.
 */
public class TableGastos {

    public static final String TABLE_NAME = "gastos";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_CUENTA = "cuenta";
    public static final String FIELD_FECHA = "fecha";
    public static final String FIELD_RAZON = "razon";
    public static final String FIELD_MONTO = "monto";
    public static final String CREATE_DB_TABLE = "create table " + TABLE_NAME + "( " +
            FIELD_ID + " integer primary key autoincrement," +
            FIELD_CUENTA + " text," +
            FIELD_FECHA + " text," +
            FIELD_RAZON + " text," +
            FIELD_MONTO + " REAL DEFAULT 0"
            + " );";

    private int id;
    private String cuenta;
    private String fecha;
    private String razon;
    private double monto;

    public TableGastos() {
    }

    public TableGastos(String cuenta, String fecha, String razon, double monto) {
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.razon = razon;
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }
}