package com.kinsock.misfinanzas.database;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Gastos", id = "_id")
public class Gasto extends Model {

    @Column(name = "cuenta")
    public Cuenta cuenta;

    @Column(name = "fecha")
    public String fecha;

    @Column(name = "razon")
    public String razon;

    @Column(name = "monto")
    public String monto;

    public Gasto() {
        super();
    }

    public Gasto(Cuenta cuenta, String fecha, String razon, String monto) {
        super();
        this.cuenta = cuenta;
        this.fecha = fecha;
        this.razon = razon;
        this.monto = monto;
    }

    public static List<Gasto> getAll() {
        // This is how you execute a query
        return new Select()
                .from(Gasto.class)
                .orderBy("fecha ASC")
                .execute();
    }

    public static List<Gasto> getAllbyCuenta(Cuenta cuenta) {
        // This is how you execute a query
        return new Select()
                .from(Gasto.class)
                .where("cuenta = ?", cuenta.getId())
                .execute();
    }

    public static String getCuenta(Cuenta cuenta) {
        Cuenta c = new Select()
                .from(Cuenta.class)
                .where("Cuenta = ?", cuenta.cuenta)
                .executeSingle();
        return c.cuenta;
    }

    public static Gasto getGasto(Long id) {
        return new Select()
                .from(Gasto.class)
                .where("_id = ?", id)
                .executeSingle();
    }

    public static double getTotalGastos() {
        double monto = 0;
        List<Gasto> gastoList = Gasto.getAll();
        for (int f = 0; f < gastoList.size(); f++) {
            monto = monto + Double.parseDouble(gastoList.get(f).monto);
        }

        return monto;
    }
}
