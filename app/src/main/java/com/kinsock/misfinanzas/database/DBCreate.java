package com.kinsock.misfinanzas.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by vick on 10/01/16.
 */
public class DBCreate extends SQLiteOpenHelper {

    public static final String DB_NAME = "contabilidad";

    private static final int SCHEME_VERSION = 1;

    public DBCreate(Context context, String name) {
        super(context, name, null, SCHEME_VERSION);

    }

    private static void populateForTest(SQLiteDatabase database) {
        for (int i = 0; i < 10; i++) {
            String title = "My Title " + i;
            String subtitle = "My subTitle " + i;
            int thumb = 1;
            String sql = "INSERT INTO " + TableBalance.TABLE_NAME + " (" +
                    TableBalance.FIELD_CUENTA + "," +
                    TableBalance.FIELD_MONTO + "," +
                    TableBalance.FIELD_ICONO +
                    ") " +
                    "VALUES ('" + title + "','" + subtitle + "'," + thumb + ")";
            database.execSQL(sql);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableIngresos.CREATE_DB_TABLE);
        db.execSQL(TableGastos.CREATE_DB_TABLE);
        db.execSQL(TableBalance.CREATE_DB_TABLE);
        populateForTest(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }

    //listar todos los ingresos
    public Cursor listar(String tabla) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "";
        if (tabla == "ingreso") {
            query = ("SELECT * FROM " + TableIngresos.TABLE_NAME + " WHERE 1 ORDER BY " + TableIngresos.FIELD_FECHA + ";");
        } else if (tabla == "gasto") {
            query = ("SELECT * FROM " + TableGastos.TABLE_NAME + " WHERE 1 ORDER BY " + TableGastos.FIELD_FECHA + ";");
        } else if (tabla == "balance") {
            query = ("SELECT * FROM " + TableBalance.TABLE_NAME + " WHERE 1 ORDER BY " + TableBalance.FIELD_ID + ";");
        }

        Cursor c = db.rawQuery(query, null);

        if (c != null) {
            c.moveToLast();
        }

        return c;
    }

    public void borrar_dato(int id, String tabla) {
        SQLiteDatabase db = getWritableDatabase();
        if (tabla == "ingreso") {
            db.execSQL("DELETE FROM " + TableIngresos.TABLE_NAME + " WHERE " + TableIngresos.FIELD_ID + " = " + id + ";");
        } else if (tabla == "gasto") {
            db.execSQL("DELETE FROM " + TableGastos.TABLE_NAME + " WHERE " + TableGastos.FIELD_ID + " = " + id + ";");
        } else if (tabla == "balance") {
            db.execSQL("DELETE FROM " + TableBalance.TABLE_NAME + " WHERE " + TableBalance.FIELD_ID + " = " + id + ";");
        }
        db.close();
    }

    public Cursor datoById(int id, String tabla) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "";

        if (tabla == "ingreso") {
            query = "SELECT * FROM " + TableIngresos.TABLE_NAME + " WHERE " + TableIngresos.FIELD_ID + " = " + id + ";";
        } else if (tabla == "gasto") {
            query = "SELECT * FROM " + TableGastos.TABLE_NAME + " WHERE " + TableGastos.FIELD_ID + " = " + id + ";";
        } else if (tabla == "balance") {
            query = "SELECT * FROM " + TableBalance.TABLE_NAME + " WHERE " + TableBalance.FIELD_ID + " = " + id + ";";
        }

        Cursor c = db.rawQuery(query, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }
}