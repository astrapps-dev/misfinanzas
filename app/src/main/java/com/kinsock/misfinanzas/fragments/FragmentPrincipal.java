package com.kinsock.misfinanzas.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kinsock.misfinanzas.MisFinanzasApplication;
import com.kinsock.misfinanzas.MisFinanzasConfig;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.intro.IntroActivity;
import com.kinsock.misfinanzas.utils.NetworkUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vick on 10/01/16.
 */
public class FragmentPrincipal extends Fragment {

    View mRootView;
    private AppBarLayout appBar;
    private TabLayout pestanas;
    private ViewPager viewPager;
    private AdView mAdView;

    public FragmentPrincipal() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_principal, container, false);
        setHasOptionsMenu(true);
        mRootView = view;
        mAdView = (AdView) mRootView.findViewById(R.id.adView);

        if (savedInstanceState == null) {
            insertarTabs(container);

            // Setear adaptador al viewpager.
            viewPager = (ViewPager) view.findViewById(R.id.pager);
            poblarViewPager(viewPager);
            pestanas.setupWithViewPager(viewPager);
        }

        return view;
    }

    private void insertarTabs(ViewGroup container) {
        View padre = (View) container.getParent();
        appBar = (AppBarLayout) padre.findViewById(R.id.appbar);
        pestanas = new TabLayout(getActivity());
        pestanas.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#FFFFFF"));
        appBar.addView(pestanas);
    }

    private void poblarViewPager(ViewPager viewPager) {
        AdaptadorSecciones adapter = new AdaptadorSecciones(getFragmentManager());
        adapter.addFragment(new FragmentBalance(), getString(R.string.tit_balance));
        adapter.addFragment(new FragmentIngresos(), getString(R.string.tit_ingresos));
        adapter.addFragment(new FragmentGastos(), getString(R.string.tit_gastos));
        viewPager.setAdapter(adapter);
        // admob
        bindDataBanner();
    }

    @Override
    public void onDestroyView() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroyView();
        appBar.removeView(pestanas);
    }

    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    private void bindDataBanner() {
        if (!MisFinanzasConfig.ADMOB_UNIT_ID.equals("") && NetworkUtility.isOnline(getActivity())) {
            // create ad view
            mAdView.setVisibility(View.VISIBLE);
            //mAdView.setAdSize(AdSize.SMART_BANNER);
            //mAdView.setAdUnitId(MisFinanzasConfig.ADMOB_UNIT_ID);

            // call ad request
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice(MisFinanzasConfig.ADMOB_TEST_DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // action bar menu
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pricipal, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // action bar menu behavior
        switch (item.getItemId()) {

            case R.id.menu_rate:
                startWebActivity(getString(R.string.app_store_uri, MisFinanzasApplication.getContext().getPackageName()));
                return true;
            case R.id.menu_tutorial:
                Intent intent = new Intent(getActivity(), IntroActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startWebActivity(String url) {
        try {
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            // can't start activity
        }
    }

    /**
     * Un {@link FragmentStatePagerAdapter} que gestiona las secciones, fragmentos y
     * títulos de las pestañas
     */
    public class AdaptadorSecciones extends FragmentStatePagerAdapter {
        private final List<Fragment> fragmentos = new ArrayList<>();
        private final List<String> titulosFragmentos = new ArrayList<>();

        public AdaptadorSecciones(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return fragmentos.get(position);
        }

        @Override
        public int getCount() {
            return fragmentos.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            fragmentos.add(fragment);
            titulosFragmentos.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titulosFragmentos.get(position);
        }

    }
}
