package com.kinsock.misfinanzas.fragments;


import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.ActividadGasto;
import com.kinsock.misfinanzas.adaptadores.GastosAdapter;
import com.kinsock.misfinanzas.database.Gasto;
import com.kinsock.misfinanzas.utils.HidingScrollListener;
import com.kinsock.misfinanzas.utils.SimpleDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentGastos extends Fragment implements GastosAdapter.ItemClickListener {

    @Bind(R.id.fabAddGasto)
    FloatingActionButton fab;

    @Bind(R.id.totalGastado)
    TextView totalGastado;
    @Bind(R.id.recicladorGastos) RecyclerView reciclador;
    private GastosAdapter adaptador;
    private LinearLayoutManager linearLayoutManager;
    private View mRootView;

    public FragmentGastos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gastos, container, false);
        ButterKnife.bind(this, view);
        mRootView = view;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ActividadGasto.class).putExtra("isEdit", false));
            }
        });

        // Preparar lista
        reciclador.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayoutManager);
        reciclador.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        GetGastos();

        SetHideOnScroll();
        return view;
    }

    private void GetGastos() {
        // Parsear con Gson
        List<Gasto> items = Gasto.getAll();
        setEmpty(items.size() > 0);
        adaptador = new GastosAdapter(items, getActivity(), this);
        reciclador.setAdapter(adaptador);
        totalGastado.setText("$" + String.valueOf(Gasto.getTotalGastos()));
    }

    @Override
    public void onResume() {
        super.onResume();
        GetGastos();
    }

    @Override
    public void onClick(View view, int position, List<Gasto> items) {
        Long idGasto = items.get(position).getId();
        startActivity(new Intent(getActivity(), ActividadGasto.class).putExtra("idGasto", idGasto).putExtra("isEdit", true));
    }

    private void SetHideOnScroll() {
        reciclador.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void hideViews() {

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) fab.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        fab.animate().translationY(fab.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void showViews() {
        fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    private void setEmpty(boolean set) {
        mRootView.findViewById(R.id.emptyView).setVisibility(set ? View.GONE : View.VISIBLE);
        mRootView.findViewById(R.id.dataView).setVisibility(set ? View.VISIBLE : View.GONE);
    }
}
