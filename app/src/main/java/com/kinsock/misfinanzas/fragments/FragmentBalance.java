package com.kinsock.misfinanzas.fragments;


import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.ActividadCuenta;
import com.kinsock.misfinanzas.adaptadores.CuentasAdapter;
import com.kinsock.misfinanzas.database.Cuenta;
import com.kinsock.misfinanzas.utils.HidingScrollListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentBalance extends Fragment implements CuentasAdapter.ItemClickListener {

    @Bind(R.id.fabAddAccount)
    FloatingActionButton fab;
    @Bind(R.id.recicladorCuentas) RecyclerView reciclador;
    private View mRootView;
    @Bind(R.id.dineroTotal) TextView dineroTotal;

    public FragmentBalance() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_balance, container, false);
        ButterKnife.bind(this, view);
        mRootView = view;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ActividadCuenta.class).putExtra("isEdit", false));
            }
        });

        // Preparar lista
        reciclador.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayoutManager);

        GetCuentas();

        SetHideOnScroll();
        return view;
    }

    private void GetCuentas() {
        // Parsear con Gson
        List<Cuenta> items = Cuenta.getAll();
        setEmpty(items.size() > 0);
        CuentasAdapter adaptador = new CuentasAdapter(items, getActivity(), this);
        reciclador.setAdapter(adaptador);

        double montoTotal = Cuenta.getDineroTotal();
        dineroTotal.setText("$" + String.valueOf(montoTotal));
        if (montoTotal > 0) {
            dineroTotal.setTextColor(getResources().getColor(R.color.color_monto_positivo));
        } else {
            dineroTotal.setTextColor(getResources().getColor(R.color.color_monto_negativo));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        GetCuentas();
    }

    @Override
    public void onClick(View view, int position, List<Cuenta> items) {
        Long idGasto = items.get(position).getId();
        startActivity(new Intent(getActivity(), ActividadCuenta.class).putExtra("idCuenta", idGasto).putExtra("isEdit", true));
    }

    private void SetHideOnScroll() {
        reciclador.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void hideViews() {

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) fab.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        fab.animate().translationY(fab.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    private void showViews() {
        fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    private void setEmpty(boolean set) {
        mRootView.findViewById(R.id.emptyView).setVisibility(set ? View.GONE : View.VISIBLE);
        mRootView.findViewById(R.id.dataView).setVisibility(set ? View.VISIBLE : View.GONE);
    }
}
