package com.kinsock.misfinanzas.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.ActividadIngreso;
import com.kinsock.misfinanzas.adaptadores.IngresosAdapter;
import com.kinsock.misfinanzas.database.Ingreso;
import com.kinsock.misfinanzas.utils.HidingScrollListener;
import com.kinsock.misfinanzas.utils.SimpleDividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentIngresos extends Fragment implements IngresosAdapter.ItemClickListener {

    @Bind(R.id.fabAddIngreso)
    FloatingActionButton mFabButton;

    @Bind(R.id.totalIngresado)
    TextView totalIngresado;
    Toolbar mToolbar;
    @Bind(R.id.recicladorIngresos) RecyclerView reciclador;
    private IngresosAdapter adaptador;
    private LinearLayoutManager linearLayoutManager;
    private View mRootView;

    public FragmentIngresos() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_ingresos, container, false);
        ButterKnife.bind(this, view);
        mRootView = view;

        mFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ActividadIngreso.class).putExtra("isEdit", false));
            }
        });

        View padre = (View) container.getParent();
        mToolbar = (Toolbar) padre.findViewById(R.id.toolbar);

        // Preparar lista
        reciclador.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        reciclador.setLayoutManager(linearLayoutManager);
        reciclador.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));

        GetIngresos(Ingreso.getAll());


        SetHideOnScroll();
        return view;
    }

    private void SetHideOnScroll() {
        reciclador.setOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void hideViews() {
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mFabButton.getLayoutParams();
        int fabBottomMargin = lp.bottomMargin;
        mFabButton.animate().translationY(mFabButton.getHeight() + fabBottomMargin).setInterpolator(new AccelerateInterpolator(2)).start();
    }

    private void showViews() {
        mFabButton.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
    }

    private void GetIngresos(List<Ingreso> items) {
        setEmpty(items.size() > 0);
        // Parsear con Gson
        adaptador = new IngresosAdapter(items, getActivity(), this);
        reciclador.setAdapter(adaptador);
        //String formattedPrice = new DecimalFormat("##,##0.00").format(String.valueOf(Ingreso.getTotalIngresos()));
        totalIngresado.setText("$" + String.valueOf(Ingreso.getTotalIngresos()));
    }

    @Override
    public void onResume() {
        super.onResume();
        GetIngresos(Ingreso.getAll());
    }

    @Override
    public void onClick(View view, int position, List<Ingreso> items) {
        Long idGasto = items.get(position).getId();
        startActivity(new Intent(getActivity(), ActividadIngreso.class).putExtra("idIngreso", idGasto).putExtra("isEdit", true));
    }

    /*public void ShowDialogAccountFilter(){
        final List<Cuenta> list = Cuenta.getAll();
        List<String> listItems = new ArrayList<String>();
        for (int f = 0; f<list.size();f++){
            listItems.add(list.get(f).cuenta);
        }
        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);
        final ArrayList seletedItems=new ArrayList();
        final boolean[] checked = new boolean[items.length];
        for (int f = 0; f<items.length;f++){
            checked[f] = true;
            seletedItems.add(f);
        }

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.tit_dialog_cuentas)
                .setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            seletedItems.add(indexSelected);
                        } else if (seletedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            seletedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on OK
                        //  You can write the code  to save the selected item here
                        List<Ingreso> ingresoList = new ArrayList<>();
                        for(int f =0; f < seletedItems.size();f++){
                            //ingresoList = Ingreso.getAllbyCuenta(list.get().getId());
                        }
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                    }
                }).create();
        dialog.show();
    }*/

    private void setEmpty(boolean set) {
        mRootView.findViewById(R.id.emptyView).setVisibility(set ? View.GONE : View.VISIBLE);
        mRootView.findViewById(R.id.dataView).setVisibility(set ? View.VISIBLE : View.GONE);
    }
}
