package com.kinsock.misfinanzas.activities.intro;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;

import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.ActividadPrincipal;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SplashScreen extends Activity {

    private static final String DB_NAME = "contabilidad";
    private boolean estado;
    @Bind(R.id.mSplashImage)
    ImageView mSplashImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash__screen);
        ButterKnife.bind(this);
        cargarPreferencias();
        ObjectAnimator splashImageAlphaAnimator = ObjectAnimator.ofFloat(mSplashImage, "Alpha", 1, 0);
        splashImageAlphaAnimator.setDuration(1500);
        splashImageAlphaAnimator.start();
        if (estado) {
            ProcesoCargaD proceso = new ProcesoCargaD();
            proceso.execute();
        } else {
            ProcesoCarga proceso = new ProcesoCarga();
            proceso.execute();
            /*DBCreate helper = new DBCreate(this, DBCreate.DB_NAME);
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues alta = new ContentValues();
            alta.put("total", 0);
            alta.put("efectivo", 0);
            alta.put("tarjeta", 0);
            alta.put("ahorro", 0);
            db.insert("balance", null, alta);
            db.close();*/
        }
    }

    private void cargarPreferencias() {
        SharedPreferences mispreferencias = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        estado = mispreferencias.getBoolean("isLoadv2", false);
    }

    private void guardarPreferencias(boolean valor) {
        SharedPreferences mispreferencias = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mispreferencias.edit();
        editor.putBoolean("isLoadv2", valor);
        editor.commit();
    }

    protected void cargarMain() {
        Intent intent = new Intent(SplashScreen.this, ActividadPrincipal.class);
        startActivity(intent);
        finish();
    }

    protected void cargarIntro() {
        Intent intent = new Intent(SplashScreen.this, IntroActivity.class);
        startActivity(intent);
        finish();
    }

    private class ProcesoCarga extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            //ejecutar alguna sincronizacion
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            cargarIntro();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private class ProcesoCargaD extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            //ejecutar alguna sincronizacion
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            cargarMain();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
