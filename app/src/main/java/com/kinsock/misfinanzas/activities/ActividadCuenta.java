package com.kinsock.misfinanzas.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.afollestad.materialdialogs.util.DialogUtils;
import com.github.ivbaranov.mli.MaterialLetterIcon;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kinsock.misfinanzas.MisFinanzasConfig;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.database.Cuenta;
import com.kinsock.misfinanzas.utils.EditTextViewEventListener;
import com.kinsock.misfinanzas.utils.NetworkUtility;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vick on 18/01/16.
 */
public class ActividadCuenta extends AppCompatActivity implements ColorChooserDialog.ColorCallback {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.nombreCuenta)
    EditText nombreCuenta;
    @Bind(R.id.eliminarCuenta)
    Button eliminarCuenta;
    @Bind(R.id.agregarCuenta)
    Button agregarCuenta;
    @Bind(R.id.colorSelec)
    MaterialLetterIcon colorSelec;
    @Bind(R.id.textViewMonto)
    TextView textViewMonto;
    String nombre;
    Cuenta cuentaEditable;
    Long idCuenta;
    //Variables verificadoras
    boolean isEdit = false;
    boolean hasColor = false;
    @Bind(R.id.adView)
    AdView mAdView;
    // color chooser dialog
    private int primaryPreselect;
    private EditTextViewEventListener eventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);
        ButterKnife.bind(this);
        isEdit = getIntent().getBooleanExtra("isEdit", false);
        SetToolbar();
        SetListeners();

        if (isEdit) {
            eliminarCuenta.setVisibility(View.VISIBLE);
            textViewMonto.setVisibility(View.VISIBLE);
            idCuenta = getIntent().getLongExtra("idCuenta", 0);
            SetDataToEdit(idCuenta);
        } else {
            primaryPreselect = DialogUtils.resolveColor(this, R.attr.colorPrimary);
        }

        // admob
        bindDataBanner();
    }

    private void SetListeners() {
        colorSelec.setInitials(false);
        colorSelec.setLetterSize(18);
        colorSelec.setLettersNumber(2);
        colorSelec.setShapeColor(R.color.colorPrimary);
        setEventListener(new EditTextViewEventListener() {
            @Override
            public void userIsTyping() {
                colorSelec.setLetter(nombreCuenta.getText().toString());
            }
        });
    }

    public void setEventListener(final EditTextViewEventListener eventListener) {
        this.eventListener = eventListener;
        setUserTypingListener();
    }

    private void setUserTypingListener() {
        nombreCuenta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    eventListener.userIsTyping();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void SetToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (isEdit) {
            setTitle(R.string.input_button_edit_cuenta);
        } else {
            setTitle(R.string.input_button_add_cuenta);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void verficarDatos(View view) {
        nombre = nombreCuenta.getText().toString();
        if (nombre.isEmpty()) {
            nombreCuenta.setError(getResources().getString(R.string.input_empty_nombre_cuenta));
        } else if (!hasColor) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage(R.string.input_empty_color_cuenta);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else {
            if (isEdit) {
                Modificar(cuentaEditable);
            } else {
                Agregar();
            }
        }
    }

    private void SetDataToEdit(Long idCuenta) {
        cuentaEditable = Cuenta.getCuenta(idCuenta);
        nombreCuenta.setText(cuentaEditable.cuenta);
        nombre = cuentaEditable.cuenta;
        primaryPreselect = cuentaEditable.color;
        colorSelec.setShapeColor(primaryPreselect);
        agregarCuenta.setText(getString(R.string.input_button_edit_cuenta));
        hasColor = true;

        // set monto
        double monto = Cuenta.getMonto(cuentaEditable);
        textViewMonto.setText("$ " + String.valueOf(monto));
        if (monto > 0) {
            textViewMonto.setTextColor(getResources().getColor(R.color.color_monto_positivo));
        } else {
            textViewMonto.setTextColor(getResources().getColor(R.color.color_monto_negativo));
        }
    }

    private void Modificar(Cuenta cuenta) {
        cuenta.cuenta = nombre;
        cuenta.color = primaryPreselect;
        cuenta.save();
        finish();
    }

    private void Agregar() {
        Cuenta cuenta = new Cuenta();
        cuenta.cuenta = nombre;
        cuenta.color = primaryPreselect;
        cuenta.save();
        finish();
    }

    public void Eliminar(View view) {
        if (Cuenta.tieneMovimientos(cuentaEditable)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setIcon(R.mipmap.ico_warning);
            alertDialog.setTitle(R.string.dialog_del_cuent_tittle);
            alertDialog.setMessage(R.string.dialog_del_cuent_mensaje);
            alertDialog.setNegativeButton(R.string.dialog_del_cuent_eliminar_todo, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Cuenta.deleteAllInAccount(cuentaEditable);
                    finish();
                }
            });
            alertDialog.setPositiveButton(R.string.dialog_del_cuent_cancelar, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertDialog.create();
            alertDialog.show();
        } else {
            cuentaEditable.delete();
            finish();
        }

    }


    public void showColorChooser(View view) {
        new ColorChooserDialog.Builder(this, R.string.color_cuenta_dialog)
                .titleSub(R.string.colors)
                .preselect(primaryPreselect)
                .customColors(R.array.custom_colors, null)
                .show();
    }

    @Override
    public void onColorSelection(@NonNull ColorChooserDialog dialog, int color) {
        primaryPreselect = color;
        hasColor = true;
        colorSelec.setShapeColor(primaryPreselect);
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    private void bindDataBanner() {
        if (!MisFinanzasConfig.ADMOB_UNIT_ID.equals("") && NetworkUtility.isOnline(this)) {
            // create ad view
            mAdView.setVisibility(View.VISIBLE);
            //mAdView.setAdSize(AdSize.SMART_BANNER);
            //mAdView.setAdUnitId(MisFinanzasConfig.ADMOB_UNIT_ID);

            // call ad request
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice(MisFinanzasConfig.ADMOB_TEST_DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
    }
}
