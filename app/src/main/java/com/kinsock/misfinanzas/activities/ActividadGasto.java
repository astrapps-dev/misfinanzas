package com.kinsock.misfinanzas.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kinsock.misfinanzas.MisFinanzasConfig;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.database.Cuenta;
import com.kinsock.misfinanzas.database.Gasto;
import com.kinsock.misfinanzas.utils.NetworkUtility;
import com.kinsock.misfinanzas.utils.TimeHelper;

import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vick on 18/01/16.
 */
public class ActividadGasto extends AppCompatActivity {

    public static final int request_code_cuenta = 69;
    // Dialogo de fecha
    static final int DIALOG_ID_FECHA = 0;
    // Variables del modelo
    public Cuenta cuenta;
    public String fecha;
    public String razon;
    public String monto;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.inputRazon)
    EditText inputRazon;
    @Bind(R.id.inputMonto)
    EditText inputMonto;
    @Bind(R.id.selecCuenta)
    Button selecCuenta;
    @Bind(R.id.selecFecha)
    Button selecFecha;
    @Bind(R.id.agregarGasto)
    Button agregarGasto;
    @Bind(R.id.eliminarGasto)
    Button eliminarGasto;
    int yearText, monthText, dayText;
    //Variables verificadoras
    boolean hasCuenta = false;
    boolean hasFecha = false;
    boolean isEdit = false;
    Long idGasto;
    Gasto gastoEditable;
    @Bind(R.id.adView)
    AdView mAdView;


    private DatePickerDialog.OnDateSetListener dpickerListener
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            yearText = year;
            monthText = monthOfYear + 1;
            dayText = dayOfMonth;

            selecFecha.setText(dayText + "-" + TimeHelper.getMonth(monthText, getApplicationContext()) + "-" + yearText);
            fecha = dayText + "-" + TimeHelper.getMonth(monthText, getApplicationContext()) + "-" + yearText;
            hasFecha = true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gasto);
        ButterKnife.bind(this);
        isEdit = getIntent().getBooleanExtra("isEdit", false);
        SetToolbar();

        if (isEdit) {
            eliminarGasto.setVisibility(View.VISIBLE);
            idGasto = getIntent().getLongExtra("idGasto", 0);
            SetDataToEdit(idGasto);
        }

        final Calendar cal = Calendar.getInstance();
        yearText = cal.get(Calendar.YEAR);
        monthText = cal.get(Calendar.MONTH);
        dayText = cal.get(Calendar.DAY_OF_MONTH);

        showDialogOnButtonClick();
        // admob
        bindDataBanner();
    }

    private void SetToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (isEdit) {
            setTitle(R.string.input_button_edit_gasto);
        } else {
            setTitle(R.string.input_button_add_gasto);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void SetDataToEdit(Long idGasto) {
        gastoEditable = Gasto.getGasto(idGasto);
        inputRazon.setText(gastoEditable.razon);
        razon = gastoEditable.razon;
        inputMonto.setText(gastoEditable.monto);
        monto = gastoEditable.monto;
        selecCuenta.setText(gastoEditable.cuenta.cuenta);
        cuenta = gastoEditable.cuenta;
        selecFecha.setText(gastoEditable.fecha);
        fecha = gastoEditable.fecha;
        agregarGasto.setText(getString(R.string.input_button_edit_gasto));
        hasCuenta = true;
        hasFecha = true;
    }

    private void ModificarGasto(Gasto gasto) {
        gasto.cuenta = cuenta;
        gasto.fecha = fecha;
        gasto.razon = razon;
        gasto.monto = monto;
        gasto.save();
        finish();
    }

    public void Eliminar(View view) {
        gastoEditable.delete();
        finish();
    }

    public void verficarDatos(View view) {
        razon = inputRazon.getText().toString();
        monto = inputMonto.getText().toString();
        if (razon.isEmpty()) {
            inputRazon.setError(getResources().getString(R.string.input_empty_razon));
        } else if (monto.isEmpty()) {
            inputMonto.setError(getResources().getString(R.string.input_empty_monto));
        } else if (!hasCuenta) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage(R.string.input_empty_cuenta);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else if (!hasFecha) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage(R.string.input_empty_fecha);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else {
            if (isEdit) {
                ModificarGasto(gastoEditable);
            } else {
                AgregarGasto();
            }
        }
    }

    private void AgregarGasto() {
        Gasto gasto = new Gasto();
        gasto.cuenta = cuenta;
        gasto.fecha = fecha;
        gasto.razon = razon;
        gasto.monto = monto;
        gasto.save();
        finish();
    }

    public void showDialogOnButtonClick() {
        selecFecha.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //noinspection deprecation
                        showDialog(DIALOG_ID_FECHA);
                    }
                }
        );
    }

    public void SelecCuenta(View view) {
        Intent intent = new Intent(this, ListarCuentas.class);
        startActivityForResult(intent, request_code_cuenta);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (resultCode == RESULT_OK) {
            if (requestCode == request_code_cuenta) {
                Long idCuenta = data.getLongExtra("cuenta", 0);
                cuenta = Cuenta.getCuenta(idCuenta);
                selecCuenta.setText(cuenta.cuenta);
                hasCuenta = true;
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID_FECHA)
            return new DatePickerDialog(this, dpickerListener, yearText, monthText, dayText);
        return null;
    }

    private void bindDataBanner() {
        if (!MisFinanzasConfig.ADMOB_UNIT_ID.equals("") && NetworkUtility.isOnline(this)) {
            // create ad view
            mAdView.setVisibility(View.VISIBLE);
            //mAdView.setAdSize(AdSize.SMART_BANNER);
            //mAdView.setAdUnitId(MisFinanzasConfig.ADMOB_UNIT_ID);

            // call ad request
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice(MisFinanzasConfig.ADMOB_TEST_DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }
}
