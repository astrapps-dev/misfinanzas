package com.kinsock.misfinanzas.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.database.Cuenta;
import com.kinsock.misfinanzas.database.Gasto;
import com.kinsock.misfinanzas.database.Ingreso;
import com.kinsock.misfinanzas.fragments.FragmentPrincipal;

public class ActividadPrincipal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actividad_principal);

        /*for(int f=0; f<100; f++){
            Cuenta efectivo = new Cuenta();
            efectivo.cuenta = "Efectivo" + String.valueOf(f);
            efectivo.color = getResources().getColor(R.color.color_10);
            efectivo.save();

            Ingreso ingreso = new Ingreso();
            ingreso.cuenta = efectivo;
            ingreso.fecha = "14-01-2016";
            ingreso.monto = "200";
            ingreso.razon = "Sueldo por muchas ventas";
            ingreso.save();

            ingreso = new Ingreso();
            ingreso.cuenta = efectivo;
            ingreso.fecha = "17-05-2016";
            ingreso.monto = "5000";
            ingreso.razon = "Comision de largo alcanse como locoo";
            ingreso.save();

            ingreso = new Ingreso();
            ingreso.cuenta = efectivo;
            ingreso.fecha = "01-03-2017";
            ingreso.monto = "650";
            ingreso.razon = "Mi sueldo";
            ingreso.save();

            Gasto gasto = new Gasto();
            gasto.cuenta = efectivo;
            gasto.fecha = "14-01-2016";
            gasto.monto = "200";
            gasto.razon = "Sueldo por muchas ventas";
            gasto.save();

            gasto = new Gasto();
            gasto.cuenta = efectivo;
            gasto.fecha = "17-05-2016";
            gasto.monto = "5000";
            gasto.razon = "Comision de largo alcanse como locoo";
            gasto.save();

            gasto = new Gasto();
            gasto.cuenta = efectivo;
            gasto.fecha = "01-03-2017";
            gasto.monto = "650";
            gasto.razon = "Mi sueldo";
            gasto.save();
        }*/

        setFragment();
    }

    private void setFragment() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Fragment fragmentoGenerico;
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentoGenerico = new FragmentPrincipal();

        fragmentManager
                .beginTransaction()
                .replace(R.id.contenedor_principal, fragmentoGenerico)
                .commit();
    }
}
