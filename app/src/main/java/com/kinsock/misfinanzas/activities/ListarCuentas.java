package com.kinsock.misfinanzas.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.kinsock.misfinanzas.MisFinanzasConfig;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.adaptadores.CuentasAdapter;
import com.kinsock.misfinanzas.database.Cuenta;
import com.kinsock.misfinanzas.utils.NetworkUtility;

import java.util.List;

public class ListarCuentas extends AppCompatActivity implements CuentasAdapter.ItemClickListener {

    private RecyclerView reciclador;
    private CuentasAdapter adaptador;
    private LinearLayoutManager linearLayoutManager;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_cuentas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAdView = (AdView) findViewById(R.id.adView);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabAddAccount);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ActividadCuenta.class).putExtra("isEdit", false));
            }
        });

        reciclador = (RecyclerView) findViewById(R.id.recicladorCuentas);
        // Preparar lista
        if (reciclador != null) {
            reciclador.setHasFixedSize(true);
        }

        linearLayoutManager = new LinearLayoutManager(this);
        reciclador.setLayoutManager(linearLayoutManager);

        GetCuentas();
        // admob
        bindDataBanner();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    private void GetCuentas() {
        // Parsear con Gson
        List<Cuenta> items = Cuenta.getAll();
        setEmpty(items.size() > 0);
        adaptador = new CuentasAdapter(items, this, this);
        reciclador.setAdapter(adaptador);
    }

    @Override
    public void onResume() {
        if (mAdView != null) {
            mAdView.resume();
        }
        super.onResume();
        GetCuentas();
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onClick(View view, int position, List<Cuenta> items) {
        Intent data = new Intent();
        data.putExtra("cuenta", items.get(position).getId());
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    private void bindDataBanner() {
        if (!MisFinanzasConfig.ADMOB_UNIT_ID.equals("") && NetworkUtility.isOnline(this)) {
            // create ad view
            mAdView.setVisibility(View.VISIBLE);
            //mAdView.setAdSize(AdSize.SMART_BANNER);
            //mAdView.setAdUnitId(MisFinanzasConfig.ADMOB_UNIT_ID);

            // call ad request
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                    .addTestDevice(MisFinanzasConfig.ADMOB_TEST_DEVICE_ID)
                    .build();
            mAdView.loadAd(adRequest);
        } else {
            mAdView.setVisibility(View.GONE);
        }
    }

    private void setEmpty(boolean set) {
        findViewById(R.id.emptyView).setVisibility(set ? View.GONE : View.VISIBLE);
        findViewById(R.id.dataView).setVisibility(set ? View.VISIBLE : View.GONE);
    }
}
