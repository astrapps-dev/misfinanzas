package com.kinsock.misfinanzas.activities.intro;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro2;
import com.kinsock.misfinanzas.R;
import com.kinsock.misfinanzas.activities.ActividadPrincipal;


public class IntroActivity extends AppIntro2 {

    @Override
    public void init(Bundle savedInstanceState) {
        addSlide(SlideIntro.newInstance(R.layout.intro_1));
        addSlide(SlideIntro.newInstance(R.layout.intro_2));
        addSlide(SlideIntro.newInstance(R.layout.intro_3));
    }

    private void loadMainActivity() {
        Intent intent = new Intent(this, ActividadPrincipal.class);
        startActivity(intent);
    }

    @Override
    public void onDonePressed() {
        SharedPreferences mispreferencias = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mispreferencias.edit();
        editor.putBoolean("isLoadv2", true);
        editor.commit();
        loadMainActivity();
        finish();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onSlideChanged() {

    }

    public void getStarted(View v) {
        loadMainActivity();
    }
}
