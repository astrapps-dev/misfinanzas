package com.kinsock.misfinanzas;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

public class MisFinanzasApplication extends Application {

    private static MisFinanzasApplication mInstance;

    private Tracker mTracker;

    public MisFinanzasApplication() {
        mInstance = this;
    }

    public static Context getContext() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }

    public synchronized Tracker getTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.setDryRun(MisFinanzasConfig.ANALYTICS_TRACKING_ID == null || MisFinanzasConfig.ANALYTICS_TRACKING_ID.equals(""));
            mTracker = analytics.newTracker(R.xml.analytics_app_tracker);
            mTracker.set("&tid", MisFinanzasConfig.ANALYTICS_TRACKING_ID);
        }
        return mTracker;
    }
}